import random
import shelve
import string
from urllib.parse import unquote
import webapp

PORT = 8033

# Esta es una plantilla para enviar el formulario y que a su vez se muestre la lista de url abajo con sus respectivos
# acortadores
PAGE_QUEST = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>The form is</p>
    <form action="/" method="POST">
      <input name="url" type="text" />
    <input type="submit" value="Submit" />
    </form>
    <p>Your list of shorted url is: 
    {Dict_shorted_urls}</p>
  </body>
</html>
"""
# Esta es una plantilla de pagina de error
PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""
# Esto abre un archivo de texto donde guardando las url acortadas como key y las url como valor
Dict_shorted_urls = shelve.open('Dict_shorted_urls')


# Esta funcion es empleada para pasar el diccionario a un formato con el cual si se mete en la plantilla PAGE_QUEST
# Se muestra de manera mas ordenadas las url y sus acortadores.
def dict_to_page():
    dict_as_page = ""
    for clave, valor in Dict_shorted_urls.items():
        dict_as_page += f'<p>Shorted: {clave} -----> URL: {valor}</p>'
    return dict_as_page


# Aqui lo que estamos haciendo es crear una clase llamada ContentApp la cual herede de webApp, donde pasamos
# el port y la ip para que ellos "creen" en servidor y lo que definimos auqi son funciones para este.
class ContentAppPut(webapp.webApp):
    # inicializamos webapp
    def __init__(self, hostname, port):
        super().__init__(hostname, port)

    # sobreescribimos parse de webApp para que nos analice lo que queremos de la peticion,
    # en este caso el nombre del recurso, contenido si es que tiene y el su metodo.
    def parse(self, request):
        result = {"recurso": request.split(' ')[1], "contenido": request.split("\n")[-1],
                  "metodo": request.split(' ')[0]}
        print(result)

        return result

    # sobreescribimos el process para describir como queremos que trate las peticiones nuestra pagina.
    def process(self, resource):
        # A este nivel miramos que metodo se emplea para la peticion
        if resource["metodo"] == "GET":
            # si nos piden mediante un GET el recurso "/" enviamos el formulatio y un diccionario que relaciona
            # la url acortada con la url.
            if resource["recurso"] == "/":
                page = PAGE_QUEST.format(Dict_shorted_urls=dict_to_page())
                code = "200 OK"
            # si ahora el recurso es una url acortada que tengamos en el diccionario, lo redirigimos
            # a la url que le corresponde
            elif resource["recurso"] in Dict_shorted_urls:
                code = "302 Found"
                page = "Location:" + Dict_shorted_urls[resource["recurso"]]
            # un GET que no sea de una / o una url acortada que tengamos cacheada, entonces enviamos una pagina
            # que notifica un error.
            else:
                page = PAGE_NOT_FOUND.format(resource=resource["recurso"])
                code = "404 Resource Not Found"
            return code, page
        elif resource["metodo"] == "POST":
            # si recibimos un POST (mayormente sucedera al rellenar nuestro formulario) que tenga como query string
            # "url=algo" entonces gestiono la peticion, de lo contrario devuelvo error.
            if "url" == resource["contenido"].split('=')[0]:
                # Algo que ocurre, es que cuando un navegador devuelve el formulario, hay caracteres especiales que se
                # decodifican de una manera distinta al original, aqui lo que hacemos es devolverlos a los originales.
                url = unquote(resource["contenido"].split('=')[1])
                # si la url introducida con empieza por "http://" o "https://" entonces le agregamos "https://".
                if not (url[0:7] == "http://" or url[0:8] == "https://"):
                    url = "https://" + url
                # si la url no esta en mi diccionario, entonces la introduzco, de lo contrario el
                # diccionario se queda igual.
                if url not in Dict_shorted_urls.values():
                    # genero una url acortada aleatoria de 5 caracteres (sin contar la /)
                    shorted_url = '/' + ''.join(random.choices(string.ascii_lowercase + string.digits, k=5))
                    # para asegurarme de que no tengo repetida esta url acortada aleatoria, miro el diccionario,
                    # si la tengo genero otra.
                    # WARNING suponemos que nunca llenaremos todas las posibles url acortadas aleatorias,
                    # de lo contrario habria que modificar k.
                    while shorted_url in Dict_shorted_urls:
                        shorted_url = ''.join(random.choices(string.ascii_lowercase + string.digits, k=5))
                    Dict_shorted_urls.update({shorted_url: url})
                page = PAGE_QUEST.format(Dict_shorted_urls=dict_to_page())
                code = "200 OK"
            else:
                page = PAGE_NOT_FOUND.format(resource=resource["recurso"])
                code = "404 Resource Not Found"
            return code, page


if __name__ == "__main__":
    webApp = ContentAppPut("localhost", PORT)
