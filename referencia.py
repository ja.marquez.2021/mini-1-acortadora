#!/usr/bin/env python3
import argparse
import http.server
import http.cookies
import random
import shelve
import string
import socketserver

PORT = 1234

PAGE_QUEST = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Tell me something to remember</p>
    <form action="/" method="GET">
      <input name="content" type="text" />
    <input type="submit" value="Submit" />
    </form>
    <p>The last content I remember is: {last_content}.</p>
  </body>
</html>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Countdown!</p>
    <p>Current count: {count}.</p>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

# Esto me crea un fichero donde guardo los contadores de cada persona
counters = shelve.open('counters')

def parse_args ():
    parser = argparse.ArgumentParser(description="Simple HTTP Server")
    parser.add_argument('-p', '--port', type=int, default=PORT,
                        help="TCP port for the server")
    args = parser.parse_args()
    return args

class Handler(http.server.BaseHTTPRequestHandler):
    #funcion por si me hacen un get
    def do_GET(self):

        global count
        # esto me da el recurso que me piden
        resource = self.path
        #si me piden /
        if resource == '/':
            #le respondemos 200 OK
            self.send_response(200)
            #Le seteamos una cookie
            cookies = http.cookies.SimpleCookie(self.headers.get('Cookie'))
            # si tiene una cookie que se llame id
            if 'id' in cookies:
                #guardo la id de esa cookie
                id = cookies['id'].value
            #si no, quiere decir que nunca se haregistrado o es la primera vex que me pide algo
            else:
                #le doy una random id
                id = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
                cookie = http.cookies.SimpleCookie()
                #le doy la id en una cookie
                cookie['id'] = id
                #se la envio
                self.send_header("Set-Cookie", cookie.output(header='', sep=''))
            #si tengo la id en mi diccionario
            if id in counters:
                #disminuyo en una unidad el contador asociado a esa id
                counters[id] = (counters[id]-1) % 6
            #como no lo tengo en mi lista la id de este usuario
            else:
                #le seteo a 5 el contador a la id de ese usuario que no tenia creando una nueva entrada en el fichero
                counters[id] = 5
            # en las plantillas de las paginas pongo el valor del contador
            page = PAGE.format(count=counters[id])
        # si no me pide /
        else:
            #no tengo ningun recurso que no sea /
            self.send_response(404)
            page = PAGE_NOT_FOUND.format(resource=resource)

        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes(page, 'utf-8'))

def main():
    #cojo los argumentos
    args = parse_args()

    try:
        with socketserver.TCPServer(("", args.port), Handler) as MyServer:
            print("serving at port", args.port)
            MyServer.serve_forever()
    except KeyboardInterrupt:
        counters.close()
        print("Finishing...")

if __name__ == "__main__":
    main()